#include "Node.h"
#include "Tasks.h"
#include <bits/stdc++.h>
using namespace std;

Node::Node()
{
    next=NULL;
    prev=NULL;
}

Node::Node(Tasks item)
{
    data=item;
}
Node* Node::getNext ()
{
    return next;
}

void Node::setNext(Node* ptr)
{
    next = ptr;
}

Node* Node::getPrev ()
{
    return prev;
}

void Node::setPrev(Node* ptr)
{
    prev = ptr;
}

Tasks  Node::getData ()
{
    return data;
}

void Node::setData (Tasks item)
{
    data = item;
}
