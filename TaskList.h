#ifndef TASKLIST_H
#define TASKLIST_H
#include "Node.h"
#include "Tasks.h"

class TaskList{
    private:
         Node *head, *tail;
          Node* temp;//da eli ana basht3'l beh w bms7o b3d kda
        int Size, SortType, SortStyle ;
    public:
        TaskList();
        void addToIndex(Tasks item, int index);
        void AddToTail(Tasks item);
        void addToHead(Tasks item);
        Node * Search(Tasks item);
        friend ostream& operator<<(ostream& stream, TaskList List);
        void addOrdered();
        int GetSize();
        void RemoveHead();
        void RemoveTail();
        void RemoveFromIndex(int index);
        void RemoveItem(Tasks item);
        bool IsPassed(Tasks item,string date);
        void RemoveWithPredicate(bool (TaskList::*predicate)(Tasks item,string criteria));
        bool IsPriorityLessThan(Tasks item,string priority);
        void WriteToFile();
        void SetHead (Node *Head) ;
        Node *GetHead () ;
        void SetTail (Node *Tail) ;
        Node *GetTail () ;
        void SwapNodes(Tasks task1, Tasks task2) ;
        bool OrderByPriority (Tasks task1, Tasks task2) ;
        bool OrderByDuration (Tasks task1, Tasks task2) ;
        bool OrderByName (Tasks task1, Tasks task2) ;
        bool OrderByDate (Tasks task1, Tasks task2) ;
        void Sort (bool (TaskList::*creteria)(Tasks task1, Tasks task2),int order) ;
        void Merge (TaskList list) ;

};

#endif // TASKLIST_H
