#ifndef TASKS_H
#define TASKS_H
#include <bits/stdc++.h>
using namespace std;

class Tasks
{
private:
        string Name;
        string Describtion;
        string Priority;
        string DueDate;
        string NumberOfDaysItTakes;
public:
    Tasks();
    void SetName(string n);
    void SetDescribtion(string d);
    void SetPriority(string p);
    void SetDueDate(string dd);
    void SetNumberOfDaysItTakes(string noofdays);
    void SetTask ();
    string GetName();
    string GetDescribtion();
    string GetPriority();
    string GetDueDate();
    string GetNumberOFDaysItTakes();
    bool operator==(Tasks item);

};

#endif // TASKS_H
