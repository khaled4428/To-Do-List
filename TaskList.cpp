#include "Tasks.h"
#include "TaskList.h"
#include "Node.h"
#include <bits/stdc++.h>
#include<cstdlib>
#include <stdlib.h>
#include<string>
using namespace std;

TaskList::TaskList()
{
   head=NULL;
   tail=NULL;
   Size=0;
   SortType = 0 ;
   SortStyle = 1 ;
}

int TaskList::GetSize()
{   Size=0;
    Node* current=head;
    while(current!=NULL){
        Size++;
        current=current->getNext();
    }
    return Size;
}

void TaskList::SetHead (Node *Head)
{
    head = Head ;
}
Node* TaskList::GetHead ()
{
    return head ;
}
void TaskList::SetTail (Node *Tail)
{
    tail = Tail ;
}
Node* TaskList::GetTail ()
{
    return tail ;
}

void TaskList::addToHead(Tasks item) {
    temp = new Node(item);
    if (head == NULL){
        head=tail=temp;
        temp->setNext(NULL);
        temp->setPrev(NULL);
    }
    else{
        temp->setNext(head);
        temp->setPrev(NULL);
        head->setPrev(temp);
        head=temp;
    }
}

void TaskList::addToIndex(Tasks item, int index)
{
    if(index<0 || index>GetSize()){
        cout<<"invalid index "<<endl;
        return ;
    }
    temp = new Node(item);
    Node* current = head;
    for (int i = 0; i < index - 1; i++)
            current = current->getNext();
        if (index == 0)
            addToHead (item);
        else if(tail == current)
            AddToTail(item);
        else {
            temp->setNext(current->getNext());
            temp->setPrev(current);
            current->getNext()->setPrev(temp);
            current->setNext(temp);
    }
}

ostream& operator<<(ostream& stream, TaskList list)
{
        Node* ptr = list.head;
        while (ptr != NULL) {
            stream << ptr->getData().GetName();
            stream<<endl;
            stream<<ptr->getData().GetDescribtion();
            stream<<endl;
            stream<<ptr->getData().GetPriority();
            stream<<endl;
            stream<<ptr->getData().GetDueDate();
            stream<<endl;
            stream<<ptr->getData().GetNumberOFDaysItTakes();
            stream<<endl;
            stream<<"================";
            stream<<endl;
            ptr = ptr->getNext();
            }
           // stream << "NULL" << endl;
         return stream;
}

void TaskList::AddToTail(Tasks item)
{
    temp=new Node(item);
    if(head==NULL){
        head=tail=temp;
        temp->setNext(NULL);
        temp->setPrev(NULL);
    }
    else{
        temp->setNext(NULL);
        temp->setPrev(tail);
        tail->setNext(temp);
        tail=temp;
    }
}

void TaskList::addOrdered()
{
    Tasks item;
    string fromfile;
    fstream file;
    file.open("MyTasks.txt");
    while(file.good()){
    getline(file,fromfile);
    item.SetName(fromfile);
    getline(file,fromfile);
    item.SetDescribtion(fromfile);
    getline(file,fromfile);
    item.SetPriority(fromfile);
    getline(file,fromfile);
    item.SetDueDate(fromfile);
    getline(file,fromfile);
    item.SetNumberOfDaysItTakes(fromfile);
    int index=0;
    Node* current=head;
    while(current!=NULL&& (current->getData().GetPriority()>item.GetPriority())){
        current=current->getNext();
        index++;
    }
    addToIndex(item,index);
    }
    file.close();
}

 void TaskList::RemoveHead()
 {
    Node* current=head;
    if(current!=NULL){
         if(current->getNext()!=NULL){
            current->getNext()->setPrev(NULL);
            head=current->getNext();
         }
         else
         {
            head = tail = NULL;
            delete current;
         }
    }
    else
        {
            cout<<"the list is empty"<<endl;
            head = tail = NULL;
            delete current;
        }
 }

void TaskList::RemoveTail()
{
   Node* current=tail;
   if(current!=NULL){
        if(current->getPrev()!=NULL)
            {
             current->getPrev()->setNext(NULL);
             tail=current->getPrev();
            }
        else{
            head = tail = NULL;
            delete current;
            }
        }
        else{
            cout<<"the list is empty"<<endl;
            head = tail = NULL;
            delete current;
            }
}

void TaskList::RemoveFromIndex(int index)
{
    if(head==NULL){
        cout<<"the list is empty"<<endl;
        return ;
        }
    if(index<0 || index>GetSize()-1){
        cout<<"invalid index"<<endl;
        return;
    }
    if(index==0)
        RemoveHead();
    else if(index==GetSize()-1)
        RemoveTail();
    else{
        Node* current=head;
        for(int i=0;i<index;i++)
            current=current->getNext();
        current->getPrev()->setNext(current->getNext());
        current->getNext()->setPrev(current->getPrev());
        delete current;
        }
}

bool TaskList::IsPassed(Tasks item,string date)
{
    string DateInItem;
    DateInItem=item.GetDueDate();
    int day,dayInItem,month,monthInItem,year,yearInItem;
    day=((date[0]-48)*10)+((date[1]-48)*1);
    dayInItem=((DateInItem[0]-48)*10)+((DateInItem[1]-48)*1);
    month=((date[3]-48)*10)+((date[4]-48)*1);
    monthInItem=((DateInItem[3]-48)*10)+((DateInItem[4]-48)*1);
    year=((date[6]-48)*1000)+((date[7]-48)*100)+((date[8]-48)*10)+((date[9]-48)*1);
    yearInItem=((DateInItem[6]-48)*1000)+((DateInItem[7]-48)*100)+((DateInItem[8]-48)*10)+((DateInItem[9]-48)*1);
    if(year>yearInItem)
        return true;
    else if(yearInItem==year&&month>monthInItem)
        return true;
    else if(yearInItem==year&&month==monthInItem&&day>dayInItem)
        return true;
    else
        return false;
}

bool TaskList::IsPriorityLessThan(Tasks item , string priority)
{
    if(item.GetPriority()<priority)
        return true;
    return false;
}

void TaskList::WriteToFile()
{   string attributes;
    Node* current=head;
    fstream file;
    file.open("SortedList.txt",ios::out|ios::trunc);
    file.seekp(0,ios::beg);
    while(current!=NULL){
        file<< current->getData().GetName() << endl;
        file<< current->getData().GetDescribtion()<< endl;
        file<< current->getData().GetPriority()<< endl;
        file<< current->getData().GetDueDate()<< endl;
        file<< current->getData().GetNumberOFDaysItTakes()<<endl;
        current=current->getNext();
    }
    file.close();
}

void TaskList::RemoveItem(Tasks item)
{
    int index=0;
    Node* current=head;
    while(current!=NULL){
        if((current->getData()==item))
              RemoveFromIndex(index);
        index++;
        current=current->getNext();
    }
}

void TaskList::RemoveWithPredicate(bool (TaskList::*predicate)(Tasks item,string criteria))
{    string criteria;
     cin>>criteria;
    Tasks item;
    Node* current=head;
    while(current!=NULL){
            item=current->getData();
            if((this->*predicate)(item,criteria)==true)
                    RemoveItem(item);
            current=current->getNext();
    }
}

Node * TaskList::Search (Tasks item)
{
    Node *current = head ;
    while(current != NULL)
    {
        if(current->getData()==item)
        {
            return current ;
        }
        current = current->getNext() ;
    }
}

void TaskList::SwapNodes(Tasks task1, Tasks task2)
{
    Node *t1 = Search(task1), *t2 = Search(task2) ;

    Node *p_t1 = t1->getPrev(), *n_t2 = t2->getNext() ;

    if (n_t2==NULL)
        {
            tail = t1 ;
            tail->setNext(NULL) ;
        }
    else
    {
        n_t2->setPrev(t1) ;                                  //handling last node swap
    }

    if (p_t1 == NULL)
         {
         head = t2 ;
        head->setPrev(NULL) ;
         }
        else                        //handling first node swap
       {
        p_t1->setNext(t2) ;
       }

    t1->setNext(n_t2) ;

    t2->setPrev(p_t1) ;

    t2->setNext(t1) ;

    t1->setPrev(t2) ;

}

bool TaskList::OrderByPriority (Tasks task1, Tasks task2)
{
    SortType = 1 ;
    Node *current = Search(task1), *next = Search(task2) ;
    stringstream obj1(current->getData().GetPriority()), obj2(next->getData().GetPriority()) ;
    int priority1, priority2;
    obj1>>priority1 ;
    obj2>>priority2 ;

     return ((priority1) < (priority2) );
}

bool TaskList::OrderByDuration (Tasks task1, Tasks task2)
{
    SortType = 2 ;
    Node *current = Search(task1), *next = Search(task2) ;
    stringstream obj1(current->getData().GetNumberOFDaysItTakes()), obj2(next->getData().GetNumberOFDaysItTakes()) ;
    int duration1, duration2;
    obj1>>duration1 ;
    obj2>>duration2 ;
     return ((duration1) < (duration2) );
}

bool TaskList::OrderByName (Tasks task1, Tasks task2)
{
    SortType = 3 ;
    Node *current = Search(task1), *next = Search(task2) ;

     return ( (current->getData().GetName()) < (next->getData().GetName()) );
}

bool TaskList::OrderByDate (Tasks task1, Tasks task2)
{
    SortType = 4 ;
    Node *next = Search(task2) ;
        return IsPassed(task1, next->getData().GetDueDate()) ;
}

void TaskList::Sort (bool (TaskList::*creteria)(Tasks task1, Tasks task2), int order)
{   SortStyle = order ;
    for(int i = 0 ; i< this->GetSize() ; i++)
    {
    Node *current = head, *next = current->getNext(), *temp ;
    while ( next !=NULL )
    {
        if ( ((this->*creteria)(current->getData(), next->getData())) ==order )
            {
                SwapNodes(current->getData(), next->getData()) ;
                temp = current ;
                current = next ;
                next = temp ;
            }
        current = current->getNext () ;
        next = current->getNext() ;
    }
    }
}

void TaskList::Merge (TaskList list)
{
    if(head==NULL){
        head=list.GetHead();
        tail=list.GetTail();
    }
    else{
    tail->setNext(list.GetHead()) ;
    (list.GetHead()->setPrev(tail)) ;
    tail = list.GetTail() ;
    }
    switch (SortType)
    {
    case 0:
        Sort(&TaskList::OrderByPriority, SortStyle) ;
        break ;
    case 1:
        Sort(&TaskList::OrderByPriority, SortStyle) ;
        break ;
    case 2:
        Sort(&TaskList::OrderByDuration, SortStyle) ;
        break ;
    case 3:
        Sort(&TaskList::OrderByName, SortStyle) ;
        break ;
    case 4:
        Sort(&TaskList::OrderByDate, SortStyle) ;
        break ;
    }

}
