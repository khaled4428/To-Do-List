#include <iostream>
#include <fstream>
#include <ostream>
#include "Tasks.h"
#include "TaskList.h"
#include "Node.h"
#include <bits/stdc++.h>
using namespace std;

int main()
{
    Tasks task ;
    TaskList T ;
    T.addOrdered();
while (true)
{
    cout<<"\t \t \t Welcome To Your To-do List Program. \n";
    cout<<"\t \t \t =================================== \n";

    cout<<"Please Choose Operation :"<<endl;
    cout<<"1-Add 2-Remove 3-Sort 4-Show Tasks 5-New List 6-Exit"<<endl;
    int choice;
    cin>>choice;
    if(choice==1) // Add
    {
        cout<<"Choose Specific Operation :"<<endl;
        cout<<"1-Add To Head  2-Add To Tail 3-Add To Index"<<endl;
        int add;
        cin>>add;
        if(add==1)
        {
            task = *(new Tasks) ;
            task.SetTask() ;
            T.addToHead(task);
            cout<< " Tasks After Editing : \n";
            cout<< " ==================== "<<endl;
            cout<<T;
        }
        if(add==2)
        {
            task = *(new Tasks) ;
            task.SetTask() ;
            T.AddToTail(task);
            cout<< " Tasks After Editing : \n";
            cout<< " ==================== "<<endl;
            cout<<T;
        }
        if(add==3)
        {
            task = *(new Tasks) ;
            task.SetTask() ;
            int index;
            cout<<"Enter Index :"<<endl;
            cin>>index;
            T.addToIndex(task,index);
            cout<< " Tasks After Editing : \n";
            cout<< " ==================== "<<endl;
            cout<<T;
        }
    }

    if (choice==2) //   Remove
    {
        cout<<"Choose Specific Operation:"<<endl;
        cout<<"1-Remove Head 2-Remove Tail 3-Remove By Index 4-Remove Item 5-Remove Passed"<<endl;
        int remov ;
        cin >> remov ;
        if(remov==1)
        {
            T.RemoveHead();
        }
        if(remov==2)
        {
            T.RemoveTail();
        }
        if(remov==3)
        {
            cout<<"Enter index :"<<endl;
            int index;
            cin>>index;
            T.RemoveFromIndex(index);
        }
        if(remov==4)   // Test to remove an item
        {
            task = *(new Tasks) ;
            task.SetTask() ;
            T.RemoveItem(task);
        }

        if(remov==5)  // Test to remove passed task
        {
            cout<<"1-remove tasks which their due date has passed"<<endl;
            cout<<"2-remove tasks which their priority is lessthan"<<endl;
            int criteria;
            cin>>criteria;
            if(criteria==1){
                cout<<"enter date"<<endl;
                T.RemoveWithPredicate(&TaskList::IsPassed);
            }
            else if(criteria==2){
                cout<<"enter priority"<<endl;
                T.RemoveWithPredicate(&TaskList::IsPriorityLessThan);
            }
        }
        cout<<T;
    }

    if(choice==3) // Test Sorting
    {
        cout<<"Choose Criteria Of Sorting :"<<endl;
        cout<<"1-By Priority 2-By Duration 3-By Name 4-By Due Date "<<endl;
        int cri,num;
        cin>>cri;
        if(cri==1)
        {
            cout<<"1 for Descending  OR  0 for Ascending "<<endl;
            cin>>num;
            if (num==0 )
            {
                T.Sort(&TaskList::OrderByPriority, num) ;
                cout<<endl;
                cout<<"Sorted Ascending By Priority: "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
            if (num ==1)
            {
                T.Sort(&TaskList::OrderByPriority,num) ;
                cout<<endl;
                cout<<"Sorted Descending By Priority: "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
        }

        if(cri==2)
        {
            cout<<"1 for Descending  OR  0 for Ascending "<<endl;
            cin>>num;
            if (num ==0 )
            {
                T.Sort(&TaskList::OrderByDuration, num) ;
                cout<<endl;
                cout<<"Sorted Ascending By Duration: "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
            if (num ==1)
            {
                T.Sort(&TaskList::OrderByDuration,num) ;
                cout<<endl;
                cout<<"Sorted Descending By Duration: "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
        }

        if(cri==3)
        {
            int num;
            cout<<"1 for Descending  OR  0 for Ascending "<<endl;
            cin>>num;
            if (num ==0 )
            {
                T.Sort(&TaskList::OrderByName,num) ;
                cout<<endl;
                cout<<"Sorted Ascending By Name : "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
            if (num ==1)
            {
                T.Sort(&TaskList::OrderByName,num) ;
                cout<<endl;
                cout<<"Sorted Descending By Name : "<<endl;
                cout <<"======================== \n";
                cout<<T;
            }
        }
        if(cri==4)
        {
            cout<<"1 for Descending  OR  0 for Ascending "<<endl;
            cin>>num;
            if (num ==0 )
            {
                T.Sort(&TaskList::OrderByDate, num) ;
                cout<<endl;
                cout<<"Sorted Ascending By Date "<<endl;
                cout <<"======================== \n";
                cout<<T;
         //       t.WriteToFile();
            }
            if (num ==1)
            {
                T.Sort(&TaskList::OrderByDate,num) ;
                cout<<endl;
                cout<<"Sorted Descending By Date "<<endl;
                cout <<"======================== \n";
                cout <<T<<endl;
            }
        }
    }

    if(choice==4) // Show
    {
        cout<<"\t \t \t \t  My To-do List \n";
        cout <<"\t \t \t \t================ \n";
        cout<<T;
    }

    if(choice==6) // Exit
    {
        cout<<"\t \t Thank You For Using Our To-do List Program. \n";
        return 0;
    }

    if (choice==5)
    {
        TaskList L ;
        int i = 1 ;
        while (true)
        {
        cout<<"\nTask "<<i<<": \n" ;
        task = *(new Tasks) ;
        task.SetTask() ;

        L.AddToTail(task) ;
        i++ ;
        cout<<"\nAnother Task? (y/n)" ; char d ; cin>>d ;
        if (d=='n' || d=='N')
            break ;
        }

        cout<<"\nThe New List: \n\n"<<L<<endl ;
        T.Merge(L) ;
    }

    T.WriteToFile();

    system("PAUSE") ;
    system("CLS") ;
}
}
