#ifndef NODE_H
#define NODE_H
#include "Tasks.h"

class Node
{
         protected:
	Tasks data;
	Node* next;
	Node* prev;
      public:
          Node ();
          Node(Tasks item);
          Node* getNext ();
          void setNext(Node* ptr);
          Node* getPrev ();
          void setPrev(Node* ptr);
          Tasks  getData ();
          void setData (Tasks item);

};

#endif // NODE_H
